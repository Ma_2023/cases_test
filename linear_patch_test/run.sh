# !/bin/bash

# 如此设计是为了避免进错目录
M3D_BINDIR="../../../M3D-C/bin"
result="result"

# 检查 $result 目录是否存在,如果不存在则创建
if [ ! -d "$result" ]; then
    mkdir "$result"
fi

# 进入 $result 目录
cd "$result"

# 获取当前目录的路径
current_dir=$(pwd)

# 运行 M3D 程序
"$current_dir/$M3D_BINDIR/M3D" ../input/M3D.input
# 检查内存是否泄漏
#  valgrind --leak-check=full "$current_dir/$M3D_BINDIR/M3D" ../input/M3D.input
